

module.exports = function(helper) {
  return {
    
    getMandatoryVariables : function(str) {
	    var variableBounds = helper.findTags(str,"<m#","#m>");

		var retObject = {};

		for(var y = 0; y < variableBounds.length; y++)
		{
			var element = variableBounds[y];
			var tagObject = helper.parseVariableTag(str.substring(element.start,element.end));
			retObject[tagObject.tag] = tagObject.value;
		}

		return retObject;
    }
    
  }
}