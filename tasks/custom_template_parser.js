/*
 * sql_express_route
 * 
 *
 * Copyright (c) 2015 Jeremy Schanz
 * Licensed under the MIT license.
 */

'use strict';

var optionsHandler = require("./parser/optionsHandler");
var helper = require("./parser/helper");

module.exports = function (grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('custom_template_parser', 'The best Grunt plugin ever.', function () {

    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
      
      tagName : '',
      template : '',
      customTemplate : '',
      files :  [],
      resultPath : '',
      parser : ''

    });

    var path = {

      task : __dirname,
      destination : process.cwd()

    };

    var parser = optionsHandler.getParserFunctions(options.parser, path,grunt);
    

    var templateMain = optionsHandler.getTemplate(options.customTemplate, options.template, path, grunt);
    
     //var files = 'tasks/routeTemplate.txt';

    // Iterate over all specified file groups.
    /*
    files.forEach(function (file) {
      // Concat specified files.
      var src = file.src.filter(function (filepath) {
        // Warn on and remove invalid source files (if nonull was set).
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      }).map(function (filepath) {
        // Read file source.
        return grunt.file.read(filepath);
      }).join(grunt.util.normalizelf(options.database));
      

      if(options.parser !== '')
      {
        var customParser = require(process.cwd()+"/"+options.parser);



      }

     */
     


      var src = grunt.file.read(options.files[0]);

      var templateMakerArea = parser.getTagSpaceArea(src,"<~TemplateMaker>","<TemplateMaker~>");

      var templateNameArray = parser.getTagSpaceArray(src,"<@"+options.tagName+">","<"+options.tagName+"@>");

      
     

      templateNameArray.forEach(function(element, index)
      {
        var template = templateMain;

        //var trueIfs = parser.getIfVariables(element);
        //var variables = parser.getVariables(element);
        var mandatory = parser.getMandatoryVariables(element); 

        var loop = parser.getLoops(element, grunt);
        
        var fileName = mandatory.fileName;

        //stop rewrite over existing file 
        if(!grunt.file.exists(fileName))
        {
            
            //remove true if statment tags 
            /*
            trueIfs.forEach(function(element, index)
            {
              
                template = template.replace(new RegExp("<if#"+element.trim()+">", 'g'),"");
                template = template.replace(new RegExp("<"+element.trim()+"#if>", 'g'),"");

            });


           
            template = parser.removeIfVariables(template);


            //find and replace variable tags 
            variables.forEach(function(element, index)
            {
                template = template.replace(new RegExp("<"+element.tag+">", 'g'),element.value);

                //grunt.log.writeln('Tag:  '+"<"+element.tag+"> : "+element.value);

            });

            retArray.push( 
            {
              typeOfTag: type,
              tagName: name,
              value: null,
              isHeader : false, 
              lineNumber : endIndex

            });
            */

           for(var j = 0; j< loop.length; j++)
           {
              var tag = loop[j];
              if(tag.typeOfTag === "LoopHeader" && tag.isHeader === true)
              {
                  helper.findTags(template,"<loop#"+tag.tagName+">", "<"+tag.tagName+"#loop>");


              }
           }


            grunt.log.writeln("Created "+ options.tagName+ " Template At " +options.resultPath+'/'+fileName);
            grunt.file.write(options.resultPath+'/'+fileName, template);
        }

      });

   
      
    
  });

};
