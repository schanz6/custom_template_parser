var helper = require("./helper");

var removeIfVariables = function(str)
{
	var variableBounds = helper.findTags(str,"<if#","#if>");	

	for(var y = variableBounds.length-1; y >= 0; y--)
	{
		var element = variableBounds[y];
		
		str = str.substring(0,element.start-4).concat(str.substring(element.end+4,str.length));
	}

	return str; 

};


var getTagSpaceArray = function(str, startTag, endTag)
{
	var tagArray = helper.findTags(str, startTag, endTag);

	var retArray = [];

	for(var y = 0; y < tagArray.length; y++)
	{
		var element = tagArray[y];
		retArray.push(str.substring(element.start,element.end));
	}

	return retArray; 


};

var getTagSpaceArea = function(str, startTag, endTag)
{
	var tagArray = getTagSpaceArray(str, startTag, endTag);

	var retString = "";

	for(var y = 0; y < tagArray.length; y++)
	{
		var element = tagArray[y];
		retString += str.substring(element.start,element.end);
	}

	return retString;


};


module.exports = {
	getTagSpaceArea : getTagSpaceArea,
	getTagSpaceArray : getTagSpaceArray,
	removeIfVariables : removeIfVariables 

};