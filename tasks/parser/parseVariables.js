var helper = require("./helper");

// Public functions
var getVariables = function(str)
{
	var variableBounds = helper.findTags(str,"<#","#>");

	var retArray = [];

	for(var y = 0; y < variableBounds.length; y++)
	{
		var element = variableBounds[y];
		retArray.push(helper.parseVariableTag(str.substring(element.start,element.end)));
	}

	return retArray;

};



var getIfVariables = function(str)
{
	var variableBounds = helper.findTags(str,"<if#","#if>");

	var retArray = [];

	for(var y = 0; y < variableBounds.length; y++)
	{
		var element = variableBounds[y];
		
		retArray = retArray.concat(str.substring(element.start,element.end).split(","));
	}

	return retArray; 

};

var getMandatoryVariables = function(str)
{
	var variableBounds = helper.findTags(str,"<m#","#m>");

	var retObject = {};

	for(var y = 0; y < variableBounds.length; y++)
	{
		var element = variableBounds[y];
		var tagObject = helper.parseVariableTag(str.substring(element.start,element.end));
		retObject[tagObject.tag] = tagObject.value;
	}

	return retObject;

};

var getLoops = function(str, grunt)
{
	

	var array = helper.getFrontLoopTag(str, "<loop#", "LoopHeader");
	array = array.concat(helper.getBackLoopTag(str, "#loop>", "LoopHeader"));

	array = array.concat(helper.getFrontLoopTag(str, "<l#", "LoopIteration"));
	array = array.concat(helper.getBackLoopTag(str, "#l>",   "LoopIteration"));

	array = array.concat(helper.getVariables(str)); 

	for(var i = 0; i < array.length; i++)
	{
		var item = array[i];

		for(var j = i+1; j < array.length; j++)
		{
			if(array[j].lineNumber < item.lineNumber)
			{
				array[i] = array[j];
				array[j] = item;
				item = array[i];  
			}
		}

	}


	return retArray;
};




module.exports = {
	
	getVariables : getVariables,
	getMandatoryVariables : getMandatoryVariables,
	getIfVariables : getIfVariables,
	getLoops : getLoops

};