

//Private functions
var getIndicesOf = function (searchStr, str) {
    var startIndex = 0, searchStrLen = searchStr.length;
    var index, indices = [];
    

    while ((index = str.indexOf(searchStr, startIndex)) > -1) 
    {
        indices.push(index);
        startIndex = index + searchStrLen;
    }
    return indices;
};




var findTags = function (str, startTag, endTag)
{
	var startIndex = getIndicesOf(startTag,str);
	var endIndex = getIndicesOf(endTag,str);
	
	var retArray = [];

	for(var y = 0; y < startIndex.length; y++)
	{
		retArray.push({ start: startIndex[y]+startTag.length , end: endIndex[y] });
	}

	return retArray;
};

var parseVariableTag = function(str)
{
	var divide = getIndicesOf(":",str)[0];

	return{
		tag : str.substring(0,divide).trim(),
		value : str.substring(divide+1,str.length-1).trim()
	};

};

var getFrontLoopTag = function(str, tag, type)
{
	var loopStart = getIndicesOf(tag,str);
	

	retArray = [];

	for(var y = 0; y < loopStart.length; y++)
	{
		var startIndex = loopStart[y]; 
		var back = str.indexOf('>', startIndex);
		var name = str.substring(startIndex+tag.length, back); 
		
		retArray.push( 
		{
			typeOfTag: type,
			tagName: name,
			value: null,
			isHeader : true, 
			lineNumber : startIndex

		});
	}

	return retArray;


};

var getBackLoopTag = function(str,tag, type)
{
	var loopBack = getIndicesOf(tag,str);
	

	retArray = [];

	for(var y = 0; y < loopBack.length; y++)
	{
		var endIndex = loopBack[y]; 
		var index = 0;
		var name = null;

		while(index<100)
		{
			if(str[endIndex-(++index)] === '<')
			{
				name = str.substring(endIndex-(--index) , endIndex); 	
				break;
			}

		}
		
		
		retArray.push( 
		{
			typeOfTag: type,
			tagName: name,
			value: null,
			isHeader : false, 
			lineNumber : endIndex

		});
	}

	return retArray;


};




var parseLoopTag = function(str)
{
	var loopStart = getIndicesOf('<loop#',str);

	retArray = [];

	for(var y = 0; y < loopStart.length; y++)
	{
		var startIndex = loopStart[y]; 
		var front = str.indexOf('>', startIndex);
		
		var name = str.substring(startIndex+6, front); 
		var backTag = "<"+name+"#loop>";
		var back = str.indexOf(backTag, front);

		retArray.push(
			{ 
				start : front+1,
				end : back,
				name : name

            });
	}

	return retArray;
	
};

var parseSubLoopTag = function(str, name)
{
	var startIndex = getIndicesOf("<#"+name+">", str);
	var endIndex = getIndicesOf("<"+name+"#>", str);

	retArray = [];

	for(var y = 0; y < startIndex.length; y++)
	{
		
		retArray.push(str.substring(startIndex[y]+name.length+3, endIndex[y]));
	}

	return retArray;
	
};

var getVariables = function(str)
{
	var variableBounds = findTags(str,"<#","#>");

	var retArray = [];

	for(var y = 0; y < variableBounds.length; y++)
	{
		var element = variableBounds[y];
	
		var tagValue = parseVariableTag(str.substring(element.start,element.end));

		retArray.push( 
		{
			typeOfTag: "variable",
			tagName: tagValue.tag,
			value: tagValue.value,
			isHeader : false, 
			lineNumber : element.start

		});
	}

	return retArray;

};





module.exports = {
	getIndicesOf : getIndicesOf,
	findTags : findTags,
	parseVariableTag : parseVariableTag,
	parseLoopTag : parseLoopTag,
	parseSubLoopTag : parseSubLoopTag,
	getFrontLoopTag : getFrontLoopTag,
	getBackLoopTag : getBackLoopTag,
	getVariables : getVariables
	

};