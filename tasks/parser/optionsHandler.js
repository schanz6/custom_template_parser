var parseVariables = require("./parseVariables");
var parseTemplates = require("./parseTemplates");
var helper = require("./helper");


var getParserFunctions = function(parser, path, grunt)
{
	
	//set function object 
	var parseFunctions =
    {
      getVariables : parseVariables.getVariables,
      getMandatoryVariables : parseVariables.getMandatoryVariables,
      getIfVariables : parseVariables.getIfVariables, 
      getLoops : parseVariables.getLoops,

      getTagSpaceArea : parseTemplates.getTagSpaceArea,
      getTagSpaceArray : parseTemplates.getTagSpaceArray,
      removeIfVariables : parseTemplates.removeIfVariables 

    };

    

    //override functions with custome parser 
    if(parser !== '')
    {

	    var customParser = require(path.destination+"/"+parser)(helper);

		
		if (typeof customParser.getIfVariables == 'function') 
		{
			 parseFunctions.getIfVariables = customParser.getIfVariables; 
		}
		if (typeof customParser.getMandatoryVariables == 'function') 
		{
			 parseFunctions.getMandatoryVariables = customParser.getMandatoryVariables; 
		}
		if (typeof customParser.getVariables == 'function') 
		{
			 parseFunctions.getVariables = customParser.getVariables; 
		}
	}
	
	return parseFunctions;


};

var getTemplate = function(customPath, templatePath, path, grunt)
{
	var selectedPath = customPath;
	var retTemplate = ""; 

	//check if custom template was entered 
	if(customPath === '')
	{
		selectedPath = templatePath+".txt"; 
		grunt.file.setBase( path.task+"/templates");
	} 
	
	if(grunt.file.exists(selectedPath))
    {
   		var retTemplate = grunt.file.read(selectedPath);
    }
    else
    {
    	//throw error 
    }

    grunt.file.setBase(path.destination);



	return retTemplate;


};



module.exports = {
	
	getParserFunctions  : getParserFunctions,
	getTemplate : getTemplate

	
};