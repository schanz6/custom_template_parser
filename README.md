# custom_template_parser

>Custom Template Parser (CTP) is a Grunt program that will auto-generate code based on tags from a target file. With little setup this will give the user the power to write just a few lines of code and have the rest auto-generated for them. 

>A simple example is a sequelize migrate file with a few CTP tags will have the power to auto-generate database object file, REST controller file, and angular routes file.

## Documentation 
Available on first release

## Release History
_(Nothing yet)_



## License
Copyright (c) 2015 Jeremy Schanz. Licensed under the MIT license.
